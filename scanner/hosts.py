'''
Módulo dedicado a las funciones de escaneo de la red
'''

from multiprocessing import Pool
from subprocess import Popen, PIPE
import ipaddress
import ifcfg
import platform
import socket

def get_rango():
    '''
    Obtiene el rango de la mascara de red
    '''
    ip = ifcfg.default_interface()['inet']
    netmask = ifcfg.default_interface()['netmask']
    n = sum(bin(int(x)).count('1') for x in netmask.split('.'))
    rango = ip+"/"+str(n)
    return rango

def ping(i):
    '''
    Ejecuta un ping a una dirección IP dada.
    Recibe por parámetro la dirección IP (i).
    Según el sistema operativo ejecuta el comando correspondiente.
    '''
    i = str(i)
    if(platform.system() == 'Windows'):
        toping = Popen(['ping', '-n', '4', '-w', '7', i], stdout=PIPE)
    else:
        toping = Popen(['ping', '-c', '4', '-W', '7', i], stdout=PIPE)
    toping.communicate()[0]
    hostalive = toping.returncode
    if hostalive == 0:
        return i
    else:
        return None

def get_nombre(ip):
    '''
    Trata de obtener el nombre del dispositivo.
    Recibe por parámetro la dirección IP (ip)
    Devuelve el nombre.
    '''
    return socket.getfqdn(ip)


def scan_ip(procesos = 100):
    '''
    Realiza el proceso de escaneado según el rango de la red.
    Recibe por parámetro la cantidad máxima de procesos simultaneos (procesos)
    y devuelve una lista con las direcciones validas.
    '''
    rango = get_rango()
    networkInterface = ipaddress.ip_interface(rango)
    network = networkInterface.network
    pool = Pool(procesos, maxtasksperchild=1)
    MapeoAsync = pool.map_async(ping, network.hosts())
    lista = MapeoAsync.get()
    lista = list(filter(None, lista))
    return lista

def get_cantidad():
    '''
    Devulve la cantidad de direcciones IP en la red.
    '''
    rango = get_rango()
    networkInterface = ipaddress.ip_interface(rango)
    network = networkInterface.network
    return network.num_addresses

if __name__ == "__main__":    
    rango = get_rango()
    networkInterface = ipaddress.ip_interface(rango)
    network = networkInterface.network
    print("Procesando",network.num_addresses,"direcciones IP")
    pool = Pool(processes=100, maxtasksperchild=1)
    MapeoAsync = pool.map_async(ping, network.hosts())
    lista = MapeoAsync.get()
    print('\x1b[2K')
    lista = list(filter(None, lista))
    print(lista)
