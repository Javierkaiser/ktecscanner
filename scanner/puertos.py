'''
Módulo dedicado a las funciones de escaneo de puertos y servicios
'''

from pyportscanner import pyscanner
from socket import *

def get_banner(ip, puerto):
    '''
    Función que trata de determinar que servicio se ejecuta en un puerto.
    Recibe la IP (ip) y el puerto (puerto).
    Devuelve el un String con la información. 
    '''
    try:
        with socket(AF_INET, SOCK_STREAM) as socketito:
            socketito.settimeout(10)
            socketito.connect((ip, puerto))
            socketito.send(b'Scaneando\r\n')
            banner = socketito.recv(20)
            return banner.decode('utf-8')
    except:
        try:
            with getservbyport(int(puerto)) as Serv:
                if Serv == '':
                    Serv = 'No disponible'
                return Serv
        except:
            return 'No disponible'
        

def scan_puertos(ip, puertos = 1000, hilos = 100):
    '''
    Función para escaneo de puertos.
    Recibe por parametros la IP (ip) y opcional una lista de puertos
    (puertos) y la cantidad máxima de procesos paralelos (hilos)
    Devuelve una tupla con la IP y un diccionario con los puertos abiertos.
    '''
    scanner = pyscanner.PortScanner(target_ports=puertos,thread_limit=hilos, verbose=False)
    host = ip
    resultado = scanner.scan(host)
    claves = list(resultado.keys())
    for x in claves:
        if resultado[x] == 'CLOSE':
            del resultado[x]
        elif resultado[x] == 'OPEN':
            resultado[x] = 'ABIERTO'
    return (ip,resultado)

if __name__ == '__main__':
    import sys
    res = scan_puertos(sys.argv[1])
    
    for x in res[1].keys():
        banner = get_banner(res[0],x)
        print('IP: {}, Puerto: {}, Estado: {}, Servicio: {}'.format(res[0],x,res[1][x],banner))