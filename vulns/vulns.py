'''
Módulo para la clase Vulners, de chequeo de vulnerabilidades.
'''

import vulners

class Vuln:
    '''
    La clase Vuln hace uso de una API del servicio Vulners.
    Se requiere una clave valida para poder utilizarlo.
    Su constructor no recibe parámetros.
    '''

    def __init__(self):
        '''
        Constructor.
        Inicializa el atritubo para almacenar la API
        '''

        self.vulners_api = ''
        
    def ingresar_key(self,key):
        '''
        Metodo para ingresar la clave y optener acceso a la API.
        Recibe por parámetro la clave (key)
        Devuelve Falso si no se pudo obtener.
        '''
        try:
            self.vulners_api = vulners.Vulners(api_key = key)
        except:
            return False

    def buscar_vuln(self,servicio):
        '''
        Metodo para buscar vulnerabilidades en base a un servicio dado.
        Recibe por parámetro el servicio o puerto (servicio).
        Devuelve el resultado obtenido.
        '''    
        try:
            resultado = self.vulners_api.search(servicio, limit=5)
        except:
            resultado = ''        
        return resultado
        
    def buscar_exploit(self,servicio):    
        '''
        Metodo para buscar exploits en base a un servicio dado.
        Recibe por parámetro el servicio o puerto (servicio).
        Devuelve el resultado obtenido.
        '''    
        try:
            resultado = self.vulners_api.searchExploit(servicio, limit=5)
        except:
            resultado = ''        
        return resultado

if __name__ == '__main__':    
    import json
    import sys
    key = input('Ingrese su clave de API')
    vulner = Vuln(key)
    resultado = vulner.buscar_vuln(sys.argv[1])
    print(json.dumps(resultado,indent = 4))