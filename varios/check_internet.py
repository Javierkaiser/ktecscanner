'''
Módulo para el chequeo de internet.
'''

import socket

def conexion():
    '''
    Función que trata de conectarse a internet.
    Devuelve True si puede conectarse o False si no puede.
    '''
    try:
        with socket.create_connection(("www.google.com", 80)) as S:
            pass
        return True
    except OSError:
        pass
    return False

if __name__ == '__main__':
    if conexion():
        print('Se pudo conectar a internet')
    else:
        print('No se pudo conectar')