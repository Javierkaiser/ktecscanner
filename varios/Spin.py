'''
Módulo para la creación de la animación del giro.
'''

import itertools
import sys
from time import sleep
from threading import Thread

class spinner:
        '''
        Clase para crear la animación del giro de la barra.
        El constructor no requiere parámetros.
        La animación se ejecuta en un proceso aparte del principal.
        '''

        def __init__(self):
                '''
                Constructor que inicializa el atributo para validar.
                '''
                self.check = False

        def spin(self,texto=''):
                '''
                Metodo principal para la animación.
                Recibe por parámetro el texto a mostrar (texto).
                '''
                spinner = itertools.cycle(['-', '\\', '|', '/'])
                print(texto, end="")
                while self.check:
                        sys.stdout.write(next(spinner))
                        sys.stdout.flush()               
                        sleep(0.1)
                        sys.stdout.write('\b')

        def iniciar(self,texto):
                '''
                Metodo para iniciar la animación en un proceso aparte.
                Recibe el texto a mostrar por parámetro (texto).
                Establece el atributo check en True.
                '''
                self.check = True
                self.t = Thread(target=self.spin, \
                        args=(texto,))
                self.t.start()
        
        def detener(self):
                '''
                Metodo para detener la animación.
                Establece el atributo check en False.
                Cierra el proceso paralelo.
                '''
                self.check = False
                self.t.join()

if __name__ == '__main__':
    pass