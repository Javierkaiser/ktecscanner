'''
Módulo para la creación de menús.
'''

import os

def limpiar():
    '''
    Limpia la pantalla, según el sistema operativo.
    '''
    os.system('cls' if os.name=='nt' else 'clear')

def cabecera():
    '''
    Dibuja la cabecera.
    '''
    limpiar()
    print('-'*80)
    print('KTecScanner - Escaner de red, puertos y vulnerabilidades')
    print('-'*80,'\n')

def primer_menu():
    '''
    Dibuja el primer menú y devuelve la opción seleccionada.
    '''
    print('Menú Principal - Seleccione una opción:')
    print(' [1] Escanear Red')
    print(' [2] Configuración')
    print('')
    print(' [0] Salir')
    print('')
    n = input('Opción: ')
    return n

def conf_menu():
    '''
    Dibuja el menú de configuración y devuelve la opción seleccionada.
    '''
    print('Menú de Configuración - Seleccione una opción:')
    print(' [1] Cantidad de hilos para procesos')
    print(' [2] Ingreso de clave de API de Vulners')
    print(' [9] Volver')
    print('')
    print(' [0] Salir')
    print('')
    n = input('Opción: ')
    return n

def procesos_menu():
    '''
    Función para consultar al usuario la cantidad máxima de procesos
    paralelos a usar.
    Devuelve la opción seleccionada.
    '''
    n = input('Ingrese la cantidad de hilos (1 a N, Números Enteros.): ')
    return n

def api_menu():
    '''
    Función para consultar al usuario la clave de API de Vulners a usar.
    Devuelve la opción seleccionada.
    '''
    n = input('Ingrese su clave de la API de Vulners: ')
    return n

def segundo_menu():
    '''
    Dibuja el segundo menú y devuelve la opción seleccionada.
    '''
    print('Menú Secundario - Seleccione una opción:')
    print(' [1] Volver a escanear la red')
    print(' [2] Ver listado de equipos encontrados')
    print(' [3] Escanear puertos')
    print(' [4] Comprobar vulnerabilidades')
    print(' [5] Generar archivo de reporte')
    print(' [9] Volver')
    print('')
    print(' [0] Salir')
    print('')
    n = input('Opción: ')
    return n

def salir():
    '''
    Sale del programa.
    '''
    quit()

if __name__ == '__main__':
    pass