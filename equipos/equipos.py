import json

class Equipo():
    '''
    Clase para el manejo de los equipos de la red.
    Almacena los datos pertinentes.
    En su constructor no requiere parametros.
    '''
    def __init__(self):
        '''
        Inicializa atributos vacios.
        '''
        self.ip = ''
        self.nombre = ''
        self.puertos = {}
        self.servicios = {}
        self.vuln = {}
        self.exploits = {}

    def get_objecto(self):
        '''
        Devulve el objecto de la clase convertido a diccionario
        '''
        obj = {'Ip':self.ip,'Nombre':self.nombre,'Puertos':self.puertos,\
        'Servicios':self.servicios,'Vulnerabilidades':self.vuln,\
            'Exploits':self.exploits}
        return obj

    def __lt__(self,otro):
        '''
        Compara los ultimos 3 números de una dirección IP para 
        determinar cual es mayor y cual menor.
        Devuelve True si la IP propia es menor.
        '''
        propia = int(self.ip.split('.')[3].zfill(3))
        ajena = int(otro.ip.split('.')[3].zfill(3))
        return propia < ajena

    def __gt__(self,otro):
        '''
        Compara los ultimos 3 números de una dirección IP para 
        determinar cual es mayor y cual menor.
        Devuelve True si la IP propia es mayor.
        '''
        propia = int(self.ip.split('.')[3].zfill(3))
        ajena = int(otro.ip.split('.')[3].zfill(3))
        return propia > ajena

    def __str__(self):
        '''
        Convierte los datos básicos a string.
        '''
        obj = 'Ip: {}, Nombre: {},\nPuertos: {},\nServicios: {}'\
            .format(self.ip,self.nombre,json.dumps(self.puertos,indent=4),\
            json.dumps(self.servicios,indent=4))
        return obj

    def __eq__(self,otro):
        '''
        Compara las direcciones IP de los objetos.
        Devuelve True si son iguales.
        '''
        return self.ip == otro.ip        

    def __ne__(self,otro):
        '''
        Compara las direcciones IP de los objetos.
        Devuelve True si no son iguales.
        '''
        return self.ip != otro.ip

    def __hash__(self):
        '''
        Devuelve un Hash de la dirección IP
        '''
        return hash(self.ip)

if __name__ == '__main__':
    pass