#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
Módulo principal del paquete KTecScanner.
Acá se reune todos los módulos individuales y se orquesta el trabajo
entre todos.
Es el encargado de la interacción con el usuario.
'''

#Imports
from archivos import manejador_archivos as MA
from equipos import equipos
from scanner import hosts,puertos
from vulns import vulns
from varios import check_internet,Spin,menus
import warnings

#Cabecera
menus.cabecera()

#Chequeo si hay internet
Spin = Spin.spinner()
Spin.iniciar('Comprobando conexión de internet: ')
conexion = check_internet.conexion()
Spin.detener()
if conexion:
    print('Disponible')
else:
    print('No disponible')

#Cargo configuraciones
Spin.iniciar('Cargando configuraciones: ')
Conf = MA.leer('./configuraciones/configuraciones.json')
Spin.detener()
if Conf == None:
    Conf = {'hilos':100,'api_key':None}
    print('Configuraciones por defecto')
else:
    print('Configuraciones cargadas')
print('\n')

#Auxiliares
temp_cabecera = 0
temp_scan = 0
temp_puertos = 0
Dispositivos = []

#Menús
while True:
    #Se realiza el chequeo para no borrar la información sobre 
    #la carga de configuraciones y el chequeo de internet
    if temp_cabecera == 0:
        temp_cabecera = 1
    else:
        menus.cabecera()

    opcion = menus.primer_menu()

    #De aqui en adelante se evaluan las opciones elegidas por el usuario.
    
    #Opción de salir
    if opcion == '0':        
        menus.salir()

    #Opción de configuraciones
    elif opcion == '2':        
        while True:
            op2 = menus.conf_menu()
            if op2 == '1':
                #Configuración de procesos
                Conf['hilos'] = int(menus.procesos_menu())
                MA.guardar(Conf,'./configuraciones/configuraciones.json')
            elif op2 == '2':
                #Configuración de clave de API
                Conf['api_key'] = menus.api_menu()
                MA.guardar(Conf,'./configuraciones/configuraciones.json')
            elif op2 == '9':
                #Se vuelve al menú anterior
                break
            elif op2 == '0':
                #Salir
                menus.salir()
            else:
                pass 

    #Segundo Menú y procesos relacionados
    elif opcion == '1':
        while True:
            menus.cabecera()

            #Escaneo de red y deshabilito que se realice automaticamente
            if temp_scan == 0:
                cant = hosts.get_cantidad()
                Spin.iniciar('Procesando '+ str(cant) + ' direcciones IP ')                
                for x in hosts.scan_ip(Conf['hilos']):
                    Equip = equipos.Equipo()
                    Equip.ip = x
                    Equip.nombre = hosts.get_nombre(x)
                    Dispositivos.append(Equip)
                Dispositivos = list(set(Dispositivos))
                Dispositivos.sort()
                Spin.detener()
                print('- Completado \n')
                temp_scan = 1
            else:
                pass            

            print('Se recomienda volver a escanear para mejores resultados')
            print('')

            op2 = menus.segundo_menu()
            if op2 == '1':
                #Habilito el re escaneo
                temp_scan = 0
                continue

            elif op2 == '2':
                #Muestro por pantalla los dispositivos encontrados
                for d in Dispositivos:
                    print(d)
                    print('\n')
                input('Presione Enter para continuar ...')

            elif op2 == '3':
                #Realiza el escaneo de puertos
                #Si no se ingresa nada o mal ingresado, se escanea por defecto
                r = input\
                    ('Ingrese puerto de inicio y final, separado por coma\n'+\
                        '(dejar vacio para escaneo por defecto): ')
                if r != '':
                    try:                        
                        rango = r.split(',')
                        Rango_int[0] = int(rango[0]) 
                        Rango_int[1] = int(rango[1]) + 1
                        rr = list(range(Rango_int[0],Rango_int[1])) 
                    except:
                        rr = 1000
                else:
                    rr = 1000
                Spin.iniciar('Escaneando puertos, espere por favor ')
                for d in Dispositivos:
                    d.puertos = puertos.scan_puertos(d.ip,rr,Conf['hilos'])[1]
                    for p in d.puertos:
                        d.servicios[p] = puertos.get_banner(d.ip,p)
                Spin.detener()
                print('\n')
                #Variable auxiliar para permitir el escaneo de 
                #vulnerabilidades correspondientes a los puertos
                temp_puertos = 1
                input('Escaneo de puertos completo.\n'+\
                    'Presione Enter para continuar ...')

            elif op2 == '4':               
                #Opción de escaneo de vulnerabilidades
                if temp_puertos == 0:
                    input('Debe escanear los puertos primero.')
                    continue
                if Conf['api_key'] != None and conexion:
                    #Manejo de advertencia para la API
                    #Ignora la advertencia por error de clave
                    with warnings.catch_warnings() as w:
                        warnings.filterwarnings("ignore")                    
                        Vuln_scaner = vulns.Vuln()
                        if Vuln_scaner.ingresar_key(Conf['api_key']) == False:
                            input('Clave no valida.\n'\
                                +'Presione Enter para continuar ...')
                            continue
                        Spin.iniciar('Escaneando vulnerabilidades ')
                        for d in Dispositivos:
                            for k,v in d.servicios.items():
                                if v != "No disponible" and v != '':           
                                    d.vuln[k] = Vuln_scaner.buscar_vuln(v)
                                    d.exploits[k] = Vuln_scaner.buscar_exploit(v)
                                else:
                                    d.vuln[k] = Vuln_scaner.buscar_vuln(str(k))
                                    d.exploits[k] = Vuln_scaner.buscar_exploit(str(k))                
                        Spin.detener()
                 
                    print('\n')
                    input('Debido a su longitud, no se muestran por pantalla'+\
                        ', solo en el reporte.\nPresione Enter para continuar ...')    
                else:
                    input('No dispone de internet y/o clave para la API.'\
                        +'\nPresione Enter para continuar ...')

            elif op2 == '5':
                #Opción para generar un archivo de reporte con los encontrado
                Disp_jsonizable = [x.get_objecto() for x in Dispositivos]
                MA.guardar(Disp_jsonizable,'./reportes/reporte.json')
                input('Reporte completo.\n'+\
                    'Presione Enter para continuar ...')

            elif op2 == '9':
                #Volvemos al menú anterior
                break

            elif op2 == '0':
                #Salir
                menus.salir()

    else:
        #Se ingreso una opción no valida
        pass