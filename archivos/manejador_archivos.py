'''
Módulo dedica a la escritura y lectura de archivos, en formato JSON
'''

import json
import os

def leer(dir_arch):
    '''
    Metodo para lectura de archivos, en formato JSON
    Recibe la ubicación del archivo como parámetro (dir_arch).
    Devuelve el contenido del archivo segun JSON
    Si el directorio o archivo no existen, los crea.
    '''
    os.makedirs(os.path.dirname(dir_arch), exist_ok=True)
    with open(dir_arch,'a+') as F:
        try:
            F.seek(0)
            dato = json.load(F)
        except:
            dato = None
    return dato

def guardar(dato,dir_arch):
    '''
    Metodo para escritura de archivos, en formato JSON
    Recibe un objeto serializable por JSON (dato) y la ubicación del archivo
    como parámetro (dir_arch).
    Si el directorio o archivo no existen, los crea.
    '''
    os.makedirs(os.path.dirname(dir_arch), exist_ok=True)
    with open(dir_arch,'w') as F:
        dato = json.dump(dato,F,indent=4)

if __name__ == '__main__':
    pass